/*******************************************************************************
 * Copyright 2023 ModalAI Inc.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * 4. The Software is used solely in conjunction with devices provided by
 *    ModalAI Inc.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ******************************************************************************/


#ifndef COMMON_H
#define COMMON_H

#include <stdint.h>

#define VOXL_COMPID		MAV_COMP_ID_VISUAL_INERTIAL_ODOMETRY
#define AP_COMP_ID		MAV_COMP_ID_AUTOPILOT1

#define MAV_CHAN_AP_OBD	0
#define MAV_CHAN_AP_GCS	1
#define MAV_CHAN_GCS	2

// client name used when connecting to servers
#define PIPE_SERVER_NAME	"voxl-mavlink-server"


// pipes publishing data from onboard stream
#define ONBOARD_CH				0
#define ONBOARD_NAME			"mavlink_onboard"
#define AP_HEARTBEAT_CH			1
#define AP_HEARTBEAT_NAME		"mavlink_ap_heartbeat"
#define SYS_STATUS_CH			2
#define SYS_STATUS_NAME			"mavlink_sys_status"
#define GPS_RAW_INT_CH			3
#define GPS_RAW_INT_NAME		"mavlink_gps_raw_int"
#define ATTITUDE_CH				4
#define ATTITUDE_NAME			"mavlink_attitude"
#define LOCAL_POS_NED_CH		5
#define LOCAL_POS_NED_NAME		"mavlink_local_position_ned"


// pipe publishing data for "normal" GCS stream from AP to GCS
#define TO_GCS_CH				6
#define TO_GCS_NAME				"mavlink_to_gcs"

// pipe publishing data coming from GCS
#define FROM_GCS_CH				7
#define FROM_GCS_NAME			"mavlink_from_gcs"

// pipe publishing list of IP addresses to connected GCS
#define GCS_IP_LIST_CH			8
#define GCS_IP_LIST_NAME		"gcs_ip_list"


// get current linux monotonics time in nanoseconds
int64_t my_time_monotonic_ns(void);

// current linux realtime clock in nanoseconds
int64_t my_time_realtime_ns(void);

#endif // end COMMON_H
