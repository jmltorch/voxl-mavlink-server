/*******************************************************************************
 * Copyright 2023 ModalAI Inc.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * 4. The Software is used solely in conjunction with devices provided by
 *    ModalAI Inc.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ******************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include "autopilot_interface.h"
#include "common.h"
#include "mavlink_services.h"


// When we respond to the autopilot's timesync requests we also send our own
// to estimate the time delta to the external autopilot.
static int64_t ap_nanos_ahead_of_voxl = 0;
static int en_ts_debug = 0;
static int time_needs_setting = 1;

void mavlink_services_handle_timesync(mavlink_message_t* msg)
{
	// keep track of the time we sent to px4 to match against the response
	static int64_t last_voxl_time_sent = -1;

	// fetch values from struct for use later
	int64_t tc1 = mavlink_msg_timesync_get_tc1(msg); // center time filled in by the responder
	int64_t ts1 = mavlink_msg_timesync_get_ts1(msg); // Time the original timesync request was sent

	uint8_t target_sysid = msg->sysid;
	// px4 sends mavlink timesync messages in units of nanoseconds!!!
	int64_t now = my_time_monotonic_ns();

	// Message originating from autopilot, timestamp and return it
	if(tc1 == 0){
		// reply with message, filling in tc1 with our own timestamp
		mavlink_message_t ret_msg;
		mavlink_msg_timesync_pack(msg->sysid, VOXL_COMPID, &ret_msg, now, ts1, \
									target_sysid, MAV_COMP_ID_AUTOPILOT1);
		autopilot_io_send_onboard_msg(&ret_msg);

		if(en_ts_debug){
			printf("timesync: now-ts1 =%4.1fms\n", (now-ts1)/1000000.0);
		}

		// also send our own request so we can keep track of px4 time too
		now = my_time_monotonic_ns(); // refresh time
		last_voxl_time_sent = now;
		mavlink_msg_timesync_pack(msg->sysid, VOXL_COMPID, &ret_msg, 0, now, \
									target_sysid, MAV_COMP_ID_AUTOPILOT1);
		autopilot_io_send_onboard_msg(&ret_msg);
		if(en_ts_debug){
			printf("sending our own ts request at %ld\n", now);
		}
		return;
	}

	// now check if the timesync packet we got back is a reponse from our
	// own request. Then calculate the offset.
	if(ts1==last_voxl_time_sent){
		// check that the round trip time wasn't too long
		int64_t rtt_ns = now - last_voxl_time_sent;

		// responses usually take 2-10 ms, and we only want the most accurate
		// ones, so slow responses longer than 3.5ms which is most of them!
		if(rtt_ns > 3500000){
			return;
		}

		// should never have gone back in time, sanity check anyway
		if(rtt_ns < 0){
			fprintf(stderr, "invalid timesync response\n");
			return;
		}

		// calculate the time offset. A quick analysis shows that for a sample
		// of points with a RRT <3.5ms, the point 0.7 between TS1 and NOW gives
		// the lowest measurement noise, so we use that!
		// TODO add a filter!
		ap_nanos_ahead_of_voxl = tc1 - (ts1 + (0.7*rtt_ns));

		if(en_ts_debug){
			printf("ap ms ahead of voxl: %10.1fms\n", ap_nanos_ahead_of_voxl/1000000.0);
		}

		// old debug prints, keep for the future in case
		//printf("%0.1f %0.1f %0.1f\n", (double)ts1/1000000.0, (double)tc1/1000000.0, (double)now/1000000.0);
		//printf("%0.1f\n", (double)ap_nanos_ahead_of_voxl/1000000.0);
		return;
	}

	// if message has tc1!=0 or a ts1 different than one we sent, then something
	// other than the autopilot, not sure what to do
	if(en_ts_debug){
		printf("unknown timesync request ts1 = %ld tc1 = %ld last_voxl_time_sent = %ld\n", \
				ts1, tc1, last_voxl_time_sent);
	}
	return;
}


void mavlink_services_en_debug_timesync(void)
{
	en_ts_debug = 1;
	return;
}

int64_t mavlink_services_get_ap_ns_ahead_of_voxl(void)
{
	return ap_nanos_ahead_of_voxl;
}



void mavlink_services_handle_system_time(mavlink_message_t* msg)
{
	// collect and consolidate information
	int64_t time_now_ns = my_time_realtime_ns();
	int64_t time_now_s = time_now_ns / 1e9;
	int64_t gps_time_us = mavlink_msg_system_time_get_time_unix_usec(msg);
	int64_t gps_time_s = gps_time_us / 1e6;

	// if we are within 10 seconds, good enough
	if(abs(time_now_s - gps_time_s)<10){
		if(time_needs_setting){
			printf("detected system time has already been set\n");
		}
		time_needs_setting = 0;
		return;
	}
	else time_needs_setting = 1;

	if(!time_needs_setting) return;

	// set time in seconds
	time_t time_to_set = gps_time_s;
	printf("seting UTC clock from GPS to: %s", asctime(gmtime(&time_to_set)));
	if(stime(&time_to_set)<0){
		perror("Failed to set system time to GPS time");
		return;
	}

	time_needs_setting = 0;

	return;
}
