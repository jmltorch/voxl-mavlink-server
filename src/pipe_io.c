/*******************************************************************************
 * Copyright 2023 ModalAI Inc.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * 4. The Software is used solely in conjunction with devices provided by
 *    ModalAI Inc.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ******************************************************************************/

#include <stdio.h>
#include <pthread.h>

#include <c_library_v2/common/mavlink.h> // include before modal_pipe !!
#include <modal_pipe_server.h>
#include <modal_pipe_interfaces.h>

#include "pipe_io.h"
#include "gcs_io.h"
#include "common.h"
#include "autopilot_interface.h"
#include "mavlink_services.h"

// save sysid of the autopilot
static uint8_t current_sysid = 0;
static int running = 0;
static int en_debug_recv = 0;
static int en_debug_send = 0;

// This isn't really used other than for debug
// low overhead so keep in case it's useful later
static void _check_sysid(mavlink_message_t* msg)
{
	// always monitor the autopilot sysid in case it changes which may happen
	// during setup and config
	if(msg->compid == MAV_COMP_ID_AUTOPILOT1 && msg->sysid != current_sysid){
		current_sysid = msg->sysid;
		printf("Detected Autopilot Mavlink SYSID %d\n", current_sysid);
	}

	return;
}


// gps data has a timestamp from the autopilot, we need to correct this
// so it is in linux monotonic time before going into MPA
static void _handle_gps_msg(mavlink_message_t* msg)
{
	// we need to duplicate the message since we are going to fix the timestamp
	// to match VOXL time since this message is going out into MPA
	int64_t px4_ts_us = mavlink_msg_gps_raw_int_get_time_usec(msg);
	int64_t new_ts_us;

	int64_t ap_ns_ahead = mavlink_services_get_ap_ns_ahead_of_voxl();

	// use our last calculated timestamp offset to PX4 if set,
	// otherwise just grab our own time.
	if(ap_ns_ahead!=0){
		new_ts_us = px4_ts_us - (ap_ns_ahead/1000);
	}
	else{
		new_ts_us = my_time_monotonic_ns()/1000;
	}

	// set the new value. NOTE this likely ruins the checksum, but we are not
	// going to repack it or unpack it, just read it out the other end of the
	// pipe directly, so this doesn't matter.
	mavlink_message_t msg_for_pipe;
	memcpy(&msg_for_pipe, msg, sizeof(mavlink_message_t));
	memcpy((void*)&_MAV_PAYLOAD(&msg_for_pipe)[0], &new_ts_us, sizeof(int64_t));
	pipe_server_write(GPS_RAW_INT_CH, (char*)&msg_for_pipe, sizeof(mavlink_message_t));
	return;
}


int pipe_io_publish_onboard_msg(mavlink_message_t* msg)
{
	// nothing to do if shutting down
	if(!running) return -1;

	_check_sysid(msg);

	// publish some of the "select" messages into MPA that we commonly use
	switch(msg->msgid){
		case MAVLINK_MSG_ID_HEARTBEAT:
			pipe_server_write(AP_HEARTBEAT_CH, (char*)msg, sizeof(mavlink_message_t));
			break;

		case MAVLINK_MSG_ID_SYS_STATUS:
			pipe_server_write(SYS_STATUS_CH, (char*)msg, sizeof(mavlink_message_t));
			break;

		case MAVLINK_MSG_ID_ATTITUDE:
			pipe_server_write(ATTITUDE_CH, (char*)msg, sizeof(mavlink_message_t));
			break;

		case MAVLINK_MSG_ID_GPS_RAW_INT:
			_handle_gps_msg(msg);
			break;

		case MAVLINK_MSG_ID_LOCAL_POSITION_NED:
			pipe_server_write(LOCAL_POS_NED_CH, (char*)msg, sizeof(mavlink_message_t));
			break;

		case MAVLINK_MSG_ID_SYSTEM_TIME:
			mavlink_services_handle_system_time(msg);
			break;

		case MAVLINK_MSG_ID_TIMESYNC:
			mavlink_services_handle_timesync(msg);
			// DO NOT FORWARD THIS ON
			// we already responded to the timesync microservice request
			// discard it now no nothing else can accidentally receive it
			// and potentially confuse the autopilot
			return 0;
			break;

		default:
			break;
	}

	if(en_debug_send){
		printf("to ob pipe  ID:%4d  compid:%4d\n", msg->msgid, msg->compid);
	}
	// publish everything to the onboard channel
	return pipe_server_write(ONBOARD_CH, (char*)msg, sizeof(mavlink_message_t));
}


int pipe_io_publish_to_gcs_msg(mavlink_message_t* msg)
{
	// nothing to do if shutting down
	if(!running) return -1;

	_check_sysid(msg);

	if(en_debug_send){
		printf("to gcs pipe ID:%4d  compid:%4d\n", msg->msgid, msg->compid);
	}

	// publish everything to the GCS channel
	return pipe_server_write(TO_GCS_CH, (char*)msg, sizeof(mavlink_message_t));
}


int pipe_io_publish_from_gcs_msg(mavlink_message_t* msg)
{
	// nothing to do if shutting down
	if(!running) return -1;

	_check_sysid(msg);

	if(en_debug_recv){
		printf("from gcs pipe ID:%4d  compid:%4d\n", msg->msgid, msg->compid);
	}

	// publish everything to the GCS channel
	return pipe_server_write(FROM_GCS_CH, (char*)msg, sizeof(mavlink_message_t));
}


// publish on both
int pipe_io_publish_to_both_channels(mavlink_message_t* msg)
{
	int ret = 0;
	ret |= pipe_io_publish_onboard_msg(msg);
	ret |= pipe_io_publish_to_gcs_msg(msg);
	return ret;
}


// send messages received through any mavlink pipe back to autopilot
static void _control_cb(int ch, char* data, int bytes, __attribute__((unused)) void* context)
{
	// validate that the data makes sense
	int n_packets;
	mavlink_message_t* msg_array = pipe_validate_mavlink_message_t(data, bytes, &n_packets);
	if(msg_array == NULL) return;

	// send every packet to autopilot or GCS depending on the pipe
	for(int i=0; i<n_packets; i++){

		if(ch==ONBOARD_CH){
			if(en_debug_send) printf("onboard  pipe  ID:%4d compid:%4d\n", msg_array[i].msgid, msg_array[i].compid);
			autopilot_io_send_onboard_msg(&msg_array[i]);
		}
		else if(ch==TO_GCS_CH){
			if(en_debug_send) printf("to gcs   pipe ID:%4d compid:%4d\n", msg_array[i].msgid, msg_array[i].compid);
			gcs_io_send_to_gcs(&msg_array[i]);
		}
		else if(ch==FROM_GCS_CH){
			if(en_debug_send) printf("from gcs pipe ID:%4d compid:%4d\n", msg_array[i].msgid, msg_array[i].compid);
			gcs_io_send_to_gcs(&msg_array[i]);
		}
		else{
			if(en_debug_send) printf("misc     pipe  ID:%4d compid:%4d\n", msg_array[i].msgid, msg_array[i].compid);
			autopilot_io_send_onboard_msg(&msg_array[i]);
		}
	}

	return;
}


int pipe_io_init(void)
{
	// all pipes allow sending data back to autopilot on the control pipe
	int flags = SERVER_FLAG_EN_CONTROL_PIPE;


	// create the pipes
	pipe_info_t info0 = { \
		.name        = ONBOARD_NAME,\
		.location    = ONBOARD_NAME,\
		.type        = "mavlink_message_t",\
		.server_name = PIPE_SERVER_NAME,\
		.size_bytes  = MAVLINK_MESSAGE_T_RECOMMENDED_PIPE_SIZE};

	pipe_server_set_control_cb(ONBOARD_CH, _control_cb, NULL);
	pipe_server_set_control_pipe_size(ONBOARD_CH, \
							MAVLINK_MESSAGE_T_RECOMMENDED_PIPE_SIZE,\
							MAVLINK_MESSAGE_T_RECOMMENDED_READ_BUF_SIZE);
	pipe_server_create(ONBOARD_CH, info0, flags);



	pipe_info_t info1 = { \
		.name        = SYS_STATUS_NAME,\
		.location    = SYS_STATUS_NAME,\
		.type        = "mavlink_message_t",\
		.server_name = PIPE_SERVER_NAME,\
		.size_bytes  = MAVLINK_MESSAGE_T_RECOMMENDED_PIPE_SIZE};

	pipe_server_set_control_cb(SYS_STATUS_CH, _control_cb, NULL);
	pipe_server_set_control_pipe_size(SYS_STATUS_CH, \
							MAVLINK_MESSAGE_T_RECOMMENDED_PIPE_SIZE,\
							MAVLINK_MESSAGE_T_RECOMMENDED_READ_BUF_SIZE);
	pipe_server_create(SYS_STATUS_CH, info1, flags);



	pipe_info_t info2 = { \
		.name        = GPS_RAW_INT_NAME,\
		.location    = GPS_RAW_INT_NAME,\
		.type        = "mavlink_message_t",\
		.server_name = PIPE_SERVER_NAME,\
		.size_bytes  = MAVLINK_MESSAGE_T_RECOMMENDED_PIPE_SIZE};

	pipe_server_set_control_cb(GPS_RAW_INT_CH, _control_cb, NULL);
	pipe_server_set_control_pipe_size(GPS_RAW_INT_CH, \
							MAVLINK_MESSAGE_T_RECOMMENDED_PIPE_SIZE,\
							MAVLINK_MESSAGE_T_RECOMMENDED_READ_BUF_SIZE);
	pipe_server_create(GPS_RAW_INT_CH, info2, flags);



	pipe_info_t info3 = { \
		.name        = ATTITUDE_NAME,\
		.location    = ATTITUDE_NAME,\
		.type        = "mavlink_message_t",\
		.server_name = PIPE_SERVER_NAME,\
		.size_bytes  = MAVLINK_MESSAGE_T_RECOMMENDED_PIPE_SIZE};

	pipe_server_set_control_cb(ATTITUDE_CH, _control_cb, NULL);
	pipe_server_set_control_pipe_size(ATTITUDE_CH, \
							MAVLINK_MESSAGE_T_RECOMMENDED_PIPE_SIZE,\
							MAVLINK_MESSAGE_T_RECOMMENDED_READ_BUF_SIZE);
	pipe_server_create(ATTITUDE_CH, info3, flags);


	pipe_info_t info4 = { \
		.name        = LOCAL_POS_NED_NAME,\
		.location    = LOCAL_POS_NED_NAME,\
		.type        = "mavlink_message_t",\
		.server_name = PIPE_SERVER_NAME,\
		.size_bytes  = MAVLINK_MESSAGE_T_RECOMMENDED_PIPE_SIZE};

	pipe_server_set_control_cb(LOCAL_POS_NED_CH, _control_cb, NULL);
	pipe_server_set_control_pipe_size(LOCAL_POS_NED_CH, \
							MAVLINK_MESSAGE_T_RECOMMENDED_PIPE_SIZE,\
							MAVLINK_MESSAGE_T_RECOMMENDED_READ_BUF_SIZE);
	pipe_server_create(LOCAL_POS_NED_CH, info4, flags);


	pipe_info_t info5 = { \
		.name        = TO_GCS_NAME,\
		.location    = TO_GCS_NAME,\
		.type        = "mavlink_message_t",\
		.server_name = PIPE_SERVER_NAME,\
		.size_bytes  = MAVLINK_MESSAGE_T_RECOMMENDED_PIPE_SIZE};

	pipe_server_set_control_cb(TO_GCS_CH, _control_cb, NULL);
	pipe_server_set_control_pipe_size(TO_GCS_CH, \
							MAVLINK_MESSAGE_T_RECOMMENDED_PIPE_SIZE,\
							MAVLINK_MESSAGE_T_RECOMMENDED_READ_BUF_SIZE);
	pipe_server_create(TO_GCS_CH, info5, flags);


	pipe_info_t info6 = { \
		.name        = FROM_GCS_NAME,\
		.location    = FROM_GCS_NAME,\
		.type        = "mavlink_message_t",\
		.server_name = PIPE_SERVER_NAME,\
		.size_bytes  = MAVLINK_MESSAGE_T_RECOMMENDED_PIPE_SIZE};

	pipe_server_set_control_cb(FROM_GCS_CH, _control_cb, NULL);
	pipe_server_set_control_pipe_size(FROM_GCS_CH, \
							MAVLINK_MESSAGE_T_RECOMMENDED_PIPE_SIZE,\
							MAVLINK_MESSAGE_T_RECOMMENDED_READ_BUF_SIZE);
	pipe_server_create(FROM_GCS_CH, info6, flags);


	pipe_info_t info7 = { \
		.name        = AP_HEARTBEAT_NAME,\
		.location    = AP_HEARTBEAT_NAME,\
		.type        = "mavlink_message_t",\
		.server_name = PIPE_SERVER_NAME,\
		.size_bytes  = MAVLINK_MESSAGE_T_RECOMMENDED_PIPE_SIZE};

	pipe_server_set_control_cb(AP_HEARTBEAT_CH, _control_cb, NULL);
	pipe_server_set_control_pipe_size(AP_HEARTBEAT_CH, \
							MAVLINK_MESSAGE_T_RECOMMENDED_PIPE_SIZE,\
							MAVLINK_MESSAGE_T_RECOMMENDED_READ_BUF_SIZE);
	pipe_server_create(AP_HEARTBEAT_CH, info7, flags);


	running = 1;

	return 0;
}


int pipe_io_stop(void)
{
	pipe_server_close(ONBOARD_CH);
	pipe_server_close(SYS_STATUS_CH);
	pipe_server_close(GPS_RAW_INT_CH);
	pipe_server_close(ATTITUDE_CH);
	pipe_server_close(LOCAL_POS_NED_CH);
	pipe_server_close(TO_GCS_CH);
	pipe_server_close(FROM_GCS_CH);
	return 0;
}


void pipe_io_en_print_debug_send(int en)
{
	en_debug_send = en;
	return;
}

void pipe_io_en_print_debug_recv(int en)
{
	en_debug_recv = en;
	return;
}
