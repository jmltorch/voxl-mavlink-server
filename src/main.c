/*******************************************************************************
 * Copyright 2023 ModalAI Inc.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * 4. The Software is used solely in conjunction with devices provided by
 *    ModalAI Inc.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ******************************************************************************/

#include <stdio.h>
#include <unistd.h>
#include <getopt.h>
#include <stdlib.h> // for exit()
#include <pthread.h>
#include <sched.h>
#include <modal_pipe.h>

#include <c_library_v2/common/mavlink.h> // include before modal_pipe.h

#include "autopilot_interface.h"
#include "gcs_io.h"
#include "pipe_io.h"
#include "config_file.h"
#include "common.h"
#include "mavlink_services.h"


#define PROCESS_NAME "voxl-mavlink-server" // to name PID file


// printed if some invalid argument was given
static void _print_usage(void)
{
	printf("\n\
voxl-mavlink-server usually runs as a systemd background service. However, for debug\n\
purposes it can be started from the command line manually with any of the following\n\
debug options. When started from the command line, voxl-mavlink-server will automatically\n\
stop the background service so you don't have to stop it manually\n\
\n\
-c, --load_config_only    Load the config file and then exit right away.\n\
                            This also adds new defaults if necessary. This is used\n\
                            by voxl-configure-voxl-px4 to make sure the config file\n\
                            is up to date without actually starting this service.\n\
-h, --help                Print this help text\n\
-i, --debug_pipe_recv     show debug info on messages coming from MPA\n\
-j, --debug_pipe_send     show debug info on messages sending to MPA\n\
-k, --debug_ap_recv       show debug info on messages coming from autopilot\n\
-l, --debug_ap_send       show debug info on messages sending to autopilot\n\
-m, --debug_gcs_recv      show debug info on messages coming from GCS\n\
-n, --debug_gcs_send      show debug info on messages sending to GCS\n\
-t, --debug_timesync      show debug info on the timesync protocol handling\n\
-z, --debug_send_recv     show debug info on all sent and received messages\n\
\n");
	return;
}


static int _parse_opts(int argc, char* argv[])
{
	static struct option long_options[] =
	{
		{"load_config_only", no_argument,   0, 'c'},
		{"help",             no_argument,   0, 'h'},
		{"debug_pipe_recv",  no_argument,   0, 'i'},
		{"debug_pipe_send",  no_argument,   0, 'j'},
		{"debug_ap_recv",    no_argument,   0, 'k'},
		{"debug_ap_send",    no_argument,   0, 'l'},
		{"debug_gcs_recv",   no_argument,   0, 'm'},
		{"debug_gcs_send",   no_argument,   0, 'n'},
		{"debug_timesync",   no_argument,   0, 't'},
		{"debug_send_recv",  no_argument,   0, 'z'},
		{0, 0, 0, 0}
	};

	while(1){
		int option_index = 0;
		int c = getopt_long(argc, argv, "chijklmntz",
							long_options, &option_index);

		if(c == -1) break; // Detect the end of the options.

		switch(c){
		case 0:
			// for long args without short equivalent that just set a flag
			// nothing left to do so just break.
			if (long_options[option_index].flag != 0) break;
			break;

		case 'c':
			config_file_load();
			exit(0);
			break;

		case 'h':
			_print_usage();
			exit(0);
			break;

		case 'i':
			pipe_io_en_print_debug_recv(1);
			break;

		case 'j':
			pipe_io_en_print_debug_send(1);
			break;

		case 'k':
			autopilot_io_en_print_debug_recv(1);
			break;

		case 'l':
			autopilot_io_en_print_debug_send(1);
			break;

		case 'm':
			gcs_io_en_print_debug_recv(1);
			break;

		case 'n':
			gcs_io_en_print_debug_send(1);
			break;

		case 't':
			mavlink_services_en_debug_timesync();
			break;

		case 'z':
			pipe_io_en_print_debug_recv(1);
			pipe_io_en_print_debug_send(1);
			autopilot_io_en_print_debug_recv(1);
			autopilot_io_en_print_debug_send(1);
			gcs_io_en_print_debug_recv(1);
			gcs_io_en_print_debug_send(1);
			break;

		default:
			_print_usage();
			return -1;
		}
	}

	return 0;
}


static void _quit(int ret)
{

	printf("Stopping autopilot io module\n");
	autopilot_io_stop();

	printf("Stopping gcs io module\n");
	gcs_io_stop();

	printf("Stopping pipe io module\n");
	pipe_io_stop();

	printf("closing remaining client pipes\n");
	pipe_client_close_all();
	printf("closing remaining server pipes\n");
	pipe_server_close_all();

	// clean up our PID file is all was successful
	printf("Removing PID file\n");
	remove_pid_file(PROCESS_NAME);
	printf("exiting\n");
	exit(ret);
	return;
}




// initializes everything then waits on signal handler to close
int main(int argc, char* argv[])
{
	if(_parse_opts(argc, argv)) return -1;

	printf("loading our own config file\n");
	if(config_file_load()) return -1;
	config_file_print();

	// make sure another instance isn't running
	// if return value is -3 then a background process is running with
	// higher privaledges and we couldn't kill it, in which case we should
	// not continue or there may be hardware conflicts. If it returned -4
	// then there was an invalid argument that needs to be fixed.
	if(kill_existing_process(PROCESS_NAME, 2.0)<-2) return -1;

	// start signal manager so we can exit cleanly
	if(enable_signal_handler()==-1){
		fprintf(stderr,"ERROR: failed to start signal manager\n");
		return -1;
	}

	// make PID file to indicate your project is running
	// due to the check made on the call to rc_kill_existing_process() above
	// we can be fairly confident there is no PID file already and we can
	// make our own safely.
	make_pid_file(PROCESS_NAME);

	// start the three modules
	if(autopilot_io_init()){
		_quit(-1);
	}
	if(gcs_io_init()){
		_quit(-1);
	}
	if(pipe_io_init()){
		_quit(-1);
	}


	// all threads started, wait for signal manager to stop it
	main_running=1;
	printf("Init complete, entering main loop\n");
	while(main_running){
		usleep(1000000);
		if(en_external_uart_ap && en_external_ap_heartbeat){
			mavlink_message_t msg;
			// TODO scan for what sysid to use instead of assuming 0
			mavlink_msg_heartbeat_pack(0, VOXL_COMPID, &msg, MAV_TYPE_ONBOARD_CONTROLLER, \
									MAV_AUTOPILOT_INVALID, 0, 0, MAV_STATE_ACTIVE);
			autopilot_io_send_onboard_msg(&msg);
		}
	}

	printf("Starting shutdown sequence\n");
	_quit(0);
	return 0;
}
