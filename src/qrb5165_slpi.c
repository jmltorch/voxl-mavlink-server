/*******************************************************************************
 * Copyright 2023 ModalAI Inc.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * 4. The Software is used solely in conjunction with devices provided by
 *    ModalAI Inc.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 ******************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <fcntl.h>
#include <pthread.h>
#include <unistd.h>	// for  close
#include <errno.h>
#include <modal_pipe_server.h>

#include "qrb5165_slpi.h"
#include "config_file.h"
#include "pipe_io.h"
#include "gcs_io.h"
#include "common.h"

#define UDP_READ_BUF_LEN	(512) // only ever needs to hold 1 mavlink message
#define MAV_CHAN			0
#define LOCAL_ADDRESS_INT	2130706433



static int running;
static int sockfd_onboard;
static int sockfd_gcs;
static struct sockaddr_in sockaddr_onboard_recv, sockaddr_onboard_send;
static struct sockaddr_in sockaddr_gcs_recv, sockaddr_gcs_send;
static struct timeval tv; // for recv timeout
static pthread_t recv_thread_id_onboard;
static pthread_t recv_thread_id_gcs;
static int print_debug_send;
static int print_debug_recv;

// keep track of if each UDP connected to the autopilot is connected
static int is_gcs_ch_connected;
static int is_onboard_ch_connected;



void qrb5165_slpi_io_en_print_debug_send(int en_print_debug)
{
	printf("Enabling UDP to slpi send debugging\n");
	print_debug_send = en_print_debug;
	return;
}

void qrb5165_slpi_io_en_print_debug_recv(int en_print_debug)
{
	printf("Enabling UDP to slpi recv debugging\n");
	print_debug_recv = en_print_debug;
	return;
}


int qrb5165_slpi_io_send_onboard_msg(mavlink_message_t* msg)
{
	// nothing to do if shutting down
	if(!running) return -1;
	if(!is_gcs_ch_connected && !is_onboard_ch_connected) return -1;

	// if this port is not connected, try to send to the other one
	if(!is_onboard_ch_connected){
		return qrb5165_slpi_io_send_gcs_msg(msg);
	}

	// basic debugging help
	if(print_debug_send){
		printf("to APob    ID:%4d\n", msg->msgid);
	}

	// unpack message into a buffer ready to send
	uint8_t buf[MAVLINK_MAX_PACKET_LEN];
	int bytes = mavlink_msg_to_send_buffer(buf, msg);

	int ret = sendto(sockfd_onboard, buf, bytes, MSG_CONFIRM, \
				(const struct sockaddr*) &sockaddr_onboard_send, sizeof(sockaddr_onboard_send));
	if(ret!=bytes){
		perror("failed to send to onboard udp socket to slpi");
		return -1;
	}

	return 0;
}


int qrb5165_slpi_io_send_gcs_msg(mavlink_message_t* msg)
{
	// nothing to do if shutting down or not connected
	if(!running) return -1;
	if(!is_gcs_ch_connected && !is_onboard_ch_connected) return -1;

	// if this port is not connected, try to send to the other one
	if(!is_gcs_ch_connected){
		return qrb5165_slpi_io_send_onboard_msg(msg);
	}

	// basic debugging help
	if(print_debug_send){
		printf("to APgcs   ID:%4d\n", msg->msgid);
	}

	// unpack message into a buffer ready to send
	uint8_t buf[MAVLINK_MAX_PACKET_LEN];
	int bytes = mavlink_msg_to_send_buffer(buf, msg);

	int ret = sendto(sockfd_gcs, buf, bytes, MSG_CONFIRM, \
				(const struct sockaddr*) &sockaddr_gcs_send, sizeof(sockaddr_gcs_send));
	if(ret!=bytes){
		perror("failed to send to gcs udp socket to slpi");
		return -1;
	}

	return 0;
}


// currently I don't think is function is actually used
// anyway, the logic should be to send to to whichever
// port is open
int qrb5165_slpi_io_send_any_msg(mavlink_message_t* msg)
{
	if(!is_onboard_ch_connected){
		qrb5165_slpi_io_send_gcs_msg(msg);
	}
	return qrb5165_slpi_io_send_onboard_msg(msg);
}



// thread to read incoming mavlink packets
static void* _recv_local_thread_func(void *is_gcs)
{
	uint8_t chan;

	if(is_gcs!=NULL){
		chan = MAV_CHAN_AP_GCS;
		printf("starting receive thread for gcs channel from autopilot\n");
	}
	else{
		chan = MAV_CHAN_AP_OBD;
		printf("starting receive thread for onboard channel from autopilot\n");
	}

	// general local variables
	int i, bytes_read;
	mavlink_message_t msg;
	mavlink_status_t status;
	memset(&status, 0, sizeof(mavlink_status_t));
	memset(&msg, 0, sizeof(mavlink_message_t));
	int msg_received = 0;
	char buf[UDP_READ_BUF_LEN];
	socklen_t len = sizeof(sockaddr_onboard_recv);

	// keep going until qrb5165_slpi_io_stop sets running to 0
	while(running){

		// Receive UDP message from flight controller, this is blocking with timeout
		if(is_gcs){
			bytes_read = recvfrom(sockfd_gcs, buf, UDP_READ_BUF_LEN, MSG_WAITALL,\
									(struct sockaddr*)&sockaddr_onboard_recv, &len);
		}
		else{
			bytes_read = recvfrom(sockfd_onboard, buf, UDP_READ_BUF_LEN, MSG_WAITALL,\
									(struct sockaddr*)&sockaddr_gcs_recv, &len);
		}


		// ignore EAGAIN error, that just means the timeout worked
		if(bytes_read < 0 && errno != EAGAIN){
			perror("ERROR: UDP recvfrom local had a problem");
			continue;
		}

		// do the mavlink byte-by-byte parsing
		msg_received = 0;
		for(i = 0; i < bytes_read; i++) {
			msg_received = mavlink_parse_char(chan, buf[i], &msg, &status);

			// debug check for dropped packets
			if(status.packet_rx_drop_count != 0){
				if(is_gcs) fprintf(stderr,"WARNING: slpi UDP gcs listener dropped a packet ");
				else fprintf(stderr,"WARNING: slpi UDP obd listener dropped a packet ");
				fprintf(stderr, "at byte%3d of%3d byte=%3d drop count:%2d msg_recv:%1d ", \
							i, bytes_read, buf[i], status.packet_rx_drop_count, msg_received);
				fprintf(stderr,"parse_err:%2d packet_idx:%3d flags:%2d state:%2d\n", \
						status.parse_error, status.packet_idx, status.flags, status.parse_state);

			}

			// msg_received indicates this byte was the end of a complete packet
			if(msg_received){
				if(print_debug_recv){
					if(is_gcs) printf("from APgcs ID:%4d\n", msg.msgid);
					else       printf("from APob  ID:%4d\n", msg.msgid);

				}

				// publish logic for the gcs channel
				if(is_gcs){
					if(!is_gcs_ch_connected){
						printf("CONNECTED to autopilot on gcs channel\n");
					}
					is_gcs_ch_connected = 1;
					pipe_io_publish_to_gcs_msg(&msg);
					gcs_io_send_to_gcs(&msg);
				}

				// publish logic for the onboard channel
				else{
					if(!is_onboard_ch_connected){
						printf("CONNECTED to autopilot on onboard channel\n");
					}
					is_onboard_ch_connected = 1;
					// if gcs channel is broken or not connected, publish
					// the onboard data to make the connection work
					if(!is_gcs_ch_connected){
						gcs_io_send_to_gcs(&msg);
						pipe_io_publish_to_gcs_msg(&msg);
					}
					pipe_io_publish_onboard_msg(&msg);
				}
			}
		}
	}

	if(is_gcs) printf("exiting autopilot slpi gcs ch read thread\n");
	else printf("exiting autopilot slpi onboard ch read thread\n");
	return NULL;
}


int qrb5165_slpi_io_init(void)
{
	// set up new sockets
	sockfd_onboard = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
	sockfd_gcs     = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);

	// set timeout for the socket
	tv.tv_sec = 0;
	tv.tv_usec = 500000;
	setsockopt(sockfd_onboard, SOL_SOCKET, SO_RCVTIMEO, (const char*)&tv, sizeof tv);
	setsockopt(sockfd_gcs,     SOL_SOCKET, SO_RCVTIMEO, (const char*)&tv, sizeof tv);

	// set up receiving port address
	sockaddr_onboard_recv.sin_family = AF_INET; // IPv4
	sockaddr_onboard_recv.sin_port = htons(onboard_port_from_autopilot);
	sockaddr_onboard_recv.sin_addr.s_addr = inet_addr("127.0.0.1");
	// set up sending port address
	sockaddr_onboard_send.sin_family = AF_INET; // IPv4
	sockaddr_onboard_send.sin_port = htons(onboard_port_to_autopilot);
	sockaddr_onboard_send.sin_addr.s_addr = inet_addr("127.0.0.1");
	// set up receiving port address
	sockaddr_gcs_recv.sin_family = AF_INET; // IPv4
	sockaddr_gcs_recv.sin_port = htons(gcs_port_from_autopilot);
	sockaddr_gcs_recv.sin_addr.s_addr = inet_addr("127.0.0.1");
	// set up sending port address
	sockaddr_gcs_send.sin_family = AF_INET; // IPv4
	sockaddr_gcs_send.sin_port = htons(gcs_port_to_autopilot);
	sockaddr_gcs_send.sin_addr.s_addr = inet_addr("127.0.0.1");

	// Bind to our receiving port, PX4 binds to it's own receiving port
	if(bind(sockfd_onboard, (const struct sockaddr *)&sockaddr_onboard_recv, sizeof(sockaddr_onboard_recv)) < 0){
		perror("onboard slpi port bind failed");
		return -1;
	}
	// Bind to our receiving port, PX4 binds to it's own receiving port
	if(bind(sockfd_gcs, (const struct sockaddr *)&sockaddr_gcs_recv, sizeof(sockaddr_gcs_recv)) < 0){
		perror("gcs slpi port bind failed");
		return -1;
	}

	// flag to the send function and receive thread that the socket is configured
	running = 1;

	// start the receiving threads
	pthread_create(&recv_thread_id_onboard, NULL, _recv_local_thread_func, (void*)0);
	pthread_create(&recv_thread_id_gcs,     NULL, _recv_local_thread_func, (void*)1);

	// let the threads start
	usleep(100000);

	return 0;
}


int qrb5165_slpi_io_stop(void)
{
	if(running==0){
		return 0;
	}

	running=0;
	pthread_join(recv_thread_id_onboard, NULL);
	pthread_join(recv_thread_id_gcs, NULL);

	printf("closing UDP sockets to slpi\n");
	if(close(sockfd_onboard)){
		fprintf(stderr, "ERROR closing onboard slpi socket\n");
		perror("");
	}
	if(close(sockfd_gcs)){
		fprintf(stderr, "ERROR closing gcs slpi socket\n");
		perror("");
	}

	printf("stopped local io to slpi autopilot\n");
	return 0;
}
